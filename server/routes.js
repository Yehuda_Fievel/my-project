const auth = require('./entities/auth/authRoutes');
const users = require('./entities/users/usersRoutes');
const books = require('./entities/books/booksRoutes');

module.exports = (app) => {
    app.use('/auth', auth);
    app.use('/users', users);
    app.use('/books', books);

    app.use((err, req, res, next) => {
        console.error(err.stack);
        res.status(err.status || 500).send(err.send);
    });

    app.all('*', (_req, res) => {
        console.log(Error('You do not belong here'));
        res.status(404).send('You do not belong here');
    });
};
