class ErrorHandler extends Error {
    /**
     * Custom class to extend Error class
     * @constructor
     * @param param
     * @param param.status - code for response status
     * @param param.message - error message
     * @param param.send - error message to send to the client
     */
    constructor({ status, message, send }) {
      super();
      this.status = status;
      this.message = message;
      this.send = send;
    }
  }
  
  module.exports = ErrorHandler;
  