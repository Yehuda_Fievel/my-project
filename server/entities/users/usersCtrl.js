const usersModels = require('../../models/users');

const get = async (req, res, next) => {
    try {
        const users = await usersModels.find();
        res.status(200).json(users);
    } catch (error) {
        next(error);
    }
};

module.exports = {
    get,
};
