const { ObjectId } = require('mongodb');
const jwt = require('../utils/jwt');

const hasAuth = (req, res, next) => {
    try {
        if (req.user) {
            return next();
        }
        res.sendStatus(401);
    } catch (error) {
        next(error);
    }
};

const authenticate = (req, res, next) => {
    try {
        const token = req.headers.authorization?.replace('Bearer ', '');
        req.user = jwt.verify(token);
        req.user._id = ObjectId(req.user._id);
        next();
    } catch (error) {
        next(error);
    }
};

const checkIsAdmin = (req, res, next) => {
    try {
        if (req.user.isAdmin) {
            return next();
        }
        res.sendStatus(401);
    } catch (error) {
        next(error);
    }
};

module.exports = {
    hasAuth,
    authenticate,
    checkIsAdmin,
};
