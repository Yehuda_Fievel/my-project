const ErrorHandler = require('../utils/ErrorHandler');

const validate = ({ schema, body, next }) => {
    const { error, value } = schema.validate(body);
    if (error) {
        return next(
            new ErrorHandler({
                status: 400,
                message: error.message,
                send: error.message,
            })
        );
    }
    next();
};

module.exports = validate;
