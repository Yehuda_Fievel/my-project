const booksModels = require('../../models/books');

const create = async (req, res, next) => {
    try {
        req.body.userId = req.user._id;
        const result = await booksModels.insertOne(req.body);
        res.status(201).json({ _id: result.insertedId });
    } catch (error) {
        next(error);
    }
};

const find = async (req, res, next) => {
    try {
        const books = await booksModels.find({ userId: req.user._id });
        res.status(200).json(books);
    } catch (error) {
        next(error);
    }
};

module.exports = {
    create,
    find,
};
