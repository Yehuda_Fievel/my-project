const express = require('express');
const router = express.Router();
const middleware = require('../middleware');
const booksValidation = require('./booksValidation');
const booksCtrl = require('./booksCtrl');

router.all('*', middleware.authenticate);

router.post('/', booksValidation.create, booksCtrl.create);

router.get('/', booksCtrl.find);

module.exports = router;
