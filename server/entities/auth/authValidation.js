const Joi = require('joi');
const validate = require('../validation');

const register = (req, res, next) => {
    const schema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        isAdmin: Joi.boolean(),
    });
    validate({ schema, body: req.body, next });
};

const login = (req, res, next) => {
    const schema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
    });
    validate({ schema, body: req.body, next });
};

module.exports = {
    register,
    login,
};
