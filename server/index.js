const express = require('express');
const path = require('path');
const cors = require('cors');
const helmet = require('helmet');
const app = express();
const port = 4000;

require('dotenv').config({ path: path.resolve(__dirname, `./${process.env.NODE_ENV}.env`) });


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});

app.use(cors());
app.use(express.json());
app.use(helmet());

require('./db').init;
require('./routes')(app);

process.on('SIGINT', (err) => console.log(err));
process.on('SIGTERM', (err) => console.log(err));
