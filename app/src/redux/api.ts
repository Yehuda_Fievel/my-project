import axios, { AxiosResponse, Method } from 'axios';

export type FetchParams = {
    url?: string;
    method: Method;
    path: string;
    data?: object;
};

const SERVER_URL = 'http://localhost:4000';

export default async function fetchData({
    url = SERVER_URL,
    method,
    path,
    data = {},
}: FetchParams) {
    try {
        const token = localStorage.getItem('token');

        const response: AxiosResponse = await axios({
            method,
            url: url + path,
            data,
            ...(token && {
                headers: {
                    Authorization: token ? 'Bearer ' + token : '',
                },
            }),
        });
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

export const loginParams = (data: object) => {
    return {
        method: 'POST',
        path: '/auth/login',
        data,
    } as FetchParams;
};

export const registerParams = (data: object) => {
    return {
        method: 'POST',
        path: '/auth/register',
        data,
    } as FetchParams;
};

export const getUsersParams = () => {
    return {
        method: 'GET',
        path: '/users',
    } as FetchParams;
};

export const saveBookParams = (data: object) => {
    return {
        method: 'POST',
        path: '/books',
        data,
    } as FetchParams;
};

export const getBooksParams = () => {
    return {
        method: 'GET',
        path: '/books',
    } as FetchParams;
};
