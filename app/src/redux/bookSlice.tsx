import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './store';
import fetchData, { getBooksParams, saveBookParams } from './api';

export type Book = {
    _id: string;
    title: string;
    author: string;
};

export interface BookState {
    book: Book;
    books: Book[];
}

const initialState: BookState = {
    books: [],
    book: {
        _id: '',
        title: '',
        author: '',
    },
};

export const saveBookApi = createAsyncThunk(
    'book/saveBook',
    async (body: object) => {
        return fetchData(saveBookParams(body));
    }
);

export const getBooksApi = createAsyncThunk('book/getBooks', async () => {
    return fetchData(getBooksParams());
});

export const bookSlice = createSlice({
    name: 'books',
    initialState,
    reducers: {
        saveBook: (state, action: PayloadAction<any>) => {
            state.book = action.payload;
        },
        resetBook: () => initialState,
    },
    extraReducers: (builder) => {
        builder
            .addCase(saveBookApi.fulfilled, (state, action) => {
                state.books.push({ ...state.book, _id: action.payload });
            })
            .addCase(saveBookApi.rejected, (state) => {
                console.error('saveBookApi.rejected');
            })
            .addCase(getBooksApi.fulfilled, (state, action) => {
                state.books = action.payload;
            })
            .addCase(getBooksApi.rejected, (state) => {
                console.error('getBooksApi.rejected');
            });
    },
});

export const { saveBook, resetBook } = bookSlice.actions;

export const selectBooks = (state: RootState) => state.books.books;

export default bookSlice.reducer;
