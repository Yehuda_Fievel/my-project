import { Modal, Input, Divider, List } from 'antd';
import useBooks from '../hooks/useBooks';
import { saveBook, saveBookApi } from '../redux/bookSlice';
import { useAppDispatch } from '../redux/hooks';

const { Search } = Input;

type Props = {
    isModalVisible: boolean;
    setIsModalVisible: (bool: boolean) => void;
};

const BookModal = (props: Props) => {
    const { findBook, books } = useBooks();
    const dispatch = useAppDispatch();

    const handleOk = () => {
        props.setIsModalVisible(false);
    };

    const handleCancel = () => {
        props.setIsModalVisible(false);
    };

    const onSearch = (value: string) => {
        findBook(value);
    };

    const onClick = (index: number) => {
        const book: any = books[index];
        const body = {
            title: book.title,
            author: book.author_name || book.contributor,
        };
        dispatch(saveBook(body));
        dispatch(saveBookApi(body));
    };

    return (
        <Modal
            title="Select a book by entering the title"
            visible={props.isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <>
                <Search
                    placeholder="input search text"
                    allowClear
                    onSearch={onSearch}
                    enterButton
                    style={{ width: 304 }}
                />
                <Divider orientation="left">Click on a book to save it</Divider>
                <List
                    bordered
                    dataSource={books}
                    renderItem={(book: any, i: number) => (
                        <List.Item onClick={() => onClick(i)}>
                            <span>{book.title}</span>
                            {/* <span>{' - '}</span> */}
                            <span>{book.author_name || book.contributor}</span>
                        </List.Item>
                    )}
                />
            </>
        </Modal>
    );
};

export default BookModal;
