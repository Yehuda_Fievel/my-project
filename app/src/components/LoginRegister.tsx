import { Button, Checkbox, Form, Input } from 'antd';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../redux/hooks';
import { login, register, selectUser } from '../redux/userSlice';

type Props = {
    type: 'register' | 'login';
    isRegister?: boolean;
};

const LoginRegister = (props: Props) => {
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();

    const navigate = useNavigate();

    useEffect(() => {
        if (user._id) {
            navigate('welcome', { replace: true });
        }
    }, [user, navigate]);

    const onFinish = (values: object) => {
        const dictionary = {
            register: () => dispatch(register(values)),
            login: () => dispatch(login(values)),
        };
        dictionary[props.type]().then(() => {
            navigate('welcome', { replace: true });
        });
    };

    return (
        <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
        >
            <Form.Item
                label="Username"
                name="username"
                rules={[
                    { required: true, message: 'Please input your username!' },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    { required: true, message: 'Please input your password!' },
                ]}
            >
                <Input.Password />
            </Form.Item>

            {props.isRegister && (
                <Form.Item
                    label="First name"
                    name="firstName"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter your first name',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
            )}

            {props.isRegister && (
                <Form.Item
                    label="Last name"
                    name="lastName"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter your last name',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
            )}

            {props.isRegister && (
                <Form.Item
                    name="isAdmin"
                    valuePropName="checked"
                    wrapperCol={{ offset: 8, span: 16 }}
                >
                    <Checkbox>I am an Admin</Checkbox>
                </Form.Item>
            )}

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default LoginRegister;
