import { Routes, Route, Navigate } from 'react-router-dom';
import { useAppSelector } from './redux/hooks';
import { selectUser } from './redux/userSlice';
import Home from './routes/Home';
import Welcome from './routes/Welcome';

const App = () => {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route
                path="/welcome"
                element={
                    <ProtectedRoute>
                        <Welcome />
                    </ProtectedRoute>
                }
            />
        </Routes>
    );
};

const ProtectedRoute = ({ children }: { children: JSX.Element }) => {
    const user = useAppSelector(selectUser);
    if (!user._id) {
        return <Navigate to="/" replace />;
    }
    return children;
};

export default App;
