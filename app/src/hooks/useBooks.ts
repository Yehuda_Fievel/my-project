import axios from 'axios';
import { useState } from 'react';

const useBooks = () => {
    const [books, setBooks] = useState([]);
    const findBook = async (q: string) => {
        setBooks([]);
        const response = await axios(
            `http://openlibrary.org/search.json?q=${q}`
        );
        setBooks(response.data.docs);
    };
    return { findBook, books };
};

export default useBooks;
